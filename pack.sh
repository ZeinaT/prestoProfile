#!/bin/bash

declare -A ARCHIVES=( ["lexicon"]="Lexicon" ["trainCorpus"]="trainCorpus" )

TARGET="${1}"

if [ -z "${TARGET}" ]
then
	echo "Syntax: ${0##*/} [OUTPUT_PROFILE_NAME]"
	exit 1
fi

[ -d "${TARGET}" ] || cp -R skel "${TARGET}"

for dir in lexicon trainCorpus
do
	RESOURCE_DIR="${TARGET}/resources/${dir}"
	mkdir -p "${RESOURCE_DIR}"
	cd "${dir}"
	zip -9 "../${RESOURCE_DIR}/${ARCHIVES[$dir]}.zip" *.csv
	cd -
done
